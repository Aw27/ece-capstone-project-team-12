import cv2
import numpy as np 

kernelOpen=np.ones((5,5))
kernelClose=np.ones((20,20))
global h, w, firstColor

#FLAGS

keepStream = False
#recordStream will check if the stream is being recorded or not 
recordStream = False

while True:
	#open camera 
	cam = cv2.VideoCapture(0)
	ret, image = cam.read()
	#resize camera output
	image = cv2.resize(image,(640,480))

	cv2.imshow("user input",image)
	#cv2.imshow("result",image) 
	#^shown towards the end 

#wait until c is pressed 
	if(cv2.waitKey(1) & 0xFF == ord('c')):
		keepStream = True 
		(x_1,y_1,w_1,h_1) = cv2.selectROI("user input", image, False, False)
		(x_2,y_2,w_2,h_2) = cv2.selectROI("user input", image, False, False)
		if w_1 > 0 or h_1 >0:
			firstColor = image[y_1:y_1+h_1, x_1:x_1+w_1]
			##cv2.imshow("cropped", firstColor)
		if w_2 > 0 or h_2 >0:
			secondColor = image[y_2:y_2+h_2, x_2:x_2+w_2]
			##cv2.imshow("cropped2", secondColor)
			#convert second color from BGR to HSV
	if keepStream == True:

		HSV_firstColor = cv2.cvtColor(firstColor,cv2.COLOR_BGR2HSV)
		#cv2.imshow("croppedHSV", HSV_firstColor)
		#calculate average HSV for first color 
		firstSum = cv2.sumElems(HSV_firstColor)
		#print(firstSum)
		h,w = HSV_firstColor.shape[:2]
		firstTotal = h * w
		firstSumAvg = np.divide(firstSum, firstTotal)
		#print(firstSumAvg)

		HSV_secondColor = cv2.cvtColor(secondColor,cv2.COLOR_BGR2HSV)
		#calculate average HSV for second color 
		secondSum = cv2.sumElems(HSV_secondColor)
		#print(secondSum)
		h_2,w_2 = HSV_secondColor.shape[:2]
		secondTotal = h_2 * w_2
		secondSumAvg = np.divide(secondSum, secondTotal)
		#print(secondSumAvg)

		#find lower and upper bounds 1
		lowerBound_1 = np.array([HSV_firstColor[0][0][0] - 10, 150, 0])
		upperBound_1 = np.array([HSV_firstColor[0][0][0] + 10, 255, 255])
		
		#find lower and upper bounds 2
		lowerBound_2 = np.array([HSV_secondColor[0][0][0] - 10, 150, 0])
		upperBound_2 = np.array([HSV_secondColor[0][0][0] + 10, 255, 255])

		#get HSV of original image
		HSV_image = cv2.cvtColor(image,cv2.COLOR_BGR2HSV)
		
		#create mask using lower and upper bounds 1
		mask_1 = cv2.inRange(HSV_image,lowerBound_1,upperBound_1)
		cv2.imshow("mask1",mask_1)

		#create mask using lower and upper bounds 2
		mask_2 = cv2.inRange(HSV_image,lowerBound_2,upperBound_2)
		cv2.imshow("mask2",mask_2)

		#morphological operation 1
		maskOpen_1=cv2.morphologyEx(mask_1,cv2.MORPH_OPEN,kernelOpen)
		maskClose_1=cv2.morphologyEx(maskOpen_1,cv2.MORPH_CLOSE,kernelClose)
		maskFinal_1=maskClose_1
		cv2.imshow("maskFinal1",maskFinal_1)

		#morphological operation 2
		maskOpen_2=cv2.morphologyEx(mask_2,cv2.MORPH_OPEN,kernelOpen)
		maskClose_2=cv2.morphologyEx(maskOpen_2,cv2.MORPH_CLOSE,kernelClose)
		maskFinal_2=maskClose_2
		cv2.imshow("maskFinal2",maskFinal_2)
		
		#RETR_EXTERNAL Retrieves only the extreme outer contour
		#CHAIN_APPROX_NONE Translates all the points from the chain code into points.
		img2,conts,h=cv2.findContours(maskFinal_1.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
		img2,conts,h=cv2.findContours(maskFinal_2.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
		
		#draw a rectangle around the object
		for i in range(len(conts)):
		#boundingRect calculates the up-right bounding rectangle of a point set.
			x_1,y_1,w_1,h_1=cv2.boundingRect(conts[i])
			cv2.rectangle(image,(x_1,y_1),(x_1+w_1,y_1+h_1),(0,0,255), 1)
			x_2,y_2,w_2,h_2=cv2.boundingRect(conts[i])
			cv2.rectangle(image,(x_2,y_2),(x_2+w_2,y_2+h_2),(0,0,255), 1)
		
		cv2.imshow("result",image)
		if recordStream == True:
			videoOutput.write(image)
		#print(maskFinal_1)
		#print(maskFinal_2)

	#if r is pressed and we already have an object to look for
	if (cv2.waitKey(1) & 0xFF == ord('r') and keepStream == True):
		print("record started...")
		#change flag status
		recordStream = True 
		#videoWriter function
		videoWriter = cv2.VideoWriter_fourcc(*'XVID')
		#20 frams per sec
		videoOutput = cv2.VideoWriter('objectfinder.avi',videoWriter, 20.0, (640,480))
		

	#if s is pressed and stream was being recorded 
	if (cv2.waitKey(1) & 0xFF == ord('s') and recordStream == True):
		print("stream saved...")
		videoOutput.release()

		#quit
	if (cv2.waitKey(1) & 0xFF == ord('q')):
		cv2.destroyAllWindows()
		break
