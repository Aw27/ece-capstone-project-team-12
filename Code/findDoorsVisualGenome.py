#!/usr/bin/python
# Iain McDonald 
# 02/12/2019
# This script was used to find all images in the visual genome database
# that have a door and open/closed in the labels file.
# This was done because the visual genome data has over 600 labels, and we only
# need to use 4. Also, the set uses PascalVOC format for labels and we need
# labels on yolo format.

import os
import sys
import re
from shutil import copyfile

doorlist = []
xmlfolder = '//home//iain//Documents//genome//xml'
photofolder = '//home//iain//Documents//VG_100K'
targetdir = '//home//iain//Documents//doorfolder'
# Walk through xml directory, find all files with door and open or closed
# and add those items to a list
for root, subFolder, files in os.walk(xmlfolder):
    i = 0
    for file in files:
        if('.xml' in file):
            if i < 5000:
                with open(os.path.join(root, file), 'r') as readfile:
                    line = readfile.read()
                    if "door" in line:
                        if "closed" in line or "open" in line:
                            #print(file)
                            i = i+1
                            doorlist.append(re.sub('.xml', '.jpg', file))
filelookup = set(doorlist) # convert to set for quick searching
#print(filelookup)
print('done finding doors')

# Find all images, copy to targetdir
for root, subFolder, files in os.walk(photofolder):
    for file in files:
        x = os.path.split(file)[1]
        if x in filelookup:
            print(x)
            copyfile(os.path.join(root, file), (targetdir + "//" + os.path.split(file)[1]))
print('done copying')
