# #####################################
# #	Object detection using openCV and pre-trained yolov3 model using IMAGE input 
# # Capstone 2018-2019
# #	Abdullah Barghouti 
# #####################################

# #import libraries
import cv2
import argparse
import numpy as np

#constant variables 
#Drawing 
BORDERTHICKNESS = 15
TEXTSIZE = 2
#Detection 
SCALINGFACTOR = 0.005
CONFIGURE_THRESHOLD = 0.5
NSMTHRESHOLD = 0.4
CONFIDENCE = 0.15

#list initialization 
classValueList = []
confidences = []
boxes = []

#defintions/ functions 
############################
def getParam():
	global args
	#taking in necessary files from command line as arguments
	ap = argparse.ArgumentParser()
	ap.add_argument('-i', '--image', required=True, help = 'path to input image')
	ap.add_argument('-c', '--config', required=True, help = 'path to yolo config file')
	ap.add_argument('-w', '--weights', required=True, help = 'path to yolo pre-trained weights')
	ap.add_argument('-cl', '--classes', required=True, help = 'path to text file containing class names')
	args = ap.parse_args()

def loadClasses():
	global classes
#read and load classes from file 
	classes = None 
	with open(args.classes, 'r') as file:
		classes = [line.strip() for line in file.readlines()]
		#number of classes available 
		classesLength = len(classes)
	return classesLength

def DeepNeueralNetowrkParameters(arguments, img, scale):
	global createdNetwork 
	global prepInput 
	createdNetwork = cv2.dnn.readNet(args.weights, args.config)
	prepInput = cv2.dnn.blobFromImage(image, SCALINGFACTOR, (416,416), (0,0,0), crop=False)
	createdNetwork.setInput(prepInput)


#taking in necessary files from command line as arguments
getParam()
#geting image 
loadClasses()

#LOOP FOR VIDEO STARTS HERE 
#read image 
image = cv2.imread(args.image)
	#image = cv2.resize(image, (1920,1080))
#get dimentions of image
height,width = image.shape[:2]
print (height)
print (width)
#deep neural network 
DeepNeueralNetowrkParameters(args,image,SCALINGFACTOR)

#forward pass. inputs passed through the network
#find name of output layer. In YOLO there are multiple layers giving their predeictions
layerName = createdNetwork.getLayerNames()
#print(layerName) #prints all layers
#get every unconnected output layer in the list of layers 
outPutLayer = [layerName[idx[0] - 1] for idx in createdNetwork.getUnconnectedOutLayers()]
#noneConnected = createdNetwork.getUnconnectedOutLayers()
#for idx in noneConnected 
	#outPytLayer = layerName[idx[0] - 1]
#print(outPutLayer) #prints output layers
layers = createdNetwork.forward(outPutLayer)

#loop for each layer 
for layer in layers:
	#loop for each detection 
	for currentDetection in layer:
		#get confidence and classID. We dont care about the first 5 elements in the list
		#first elements are used for drawing the bounding corners
		scores = currentDetection[5:]
		classValue = np.argmax(scores)
		confidenceScore = scores[classValue]
		# print(confidenceScore)
		#look for object
		if confidenceScore > CONFIDENCE:
			#if object was detected ith confidence score hgiher than we specify,
			#find out where it is
			#bounding corners 
			centerHorizontal = int(currentDetection[0] * width)
			centerVertical = int(currentDetection[1] * height)
			cornerWidth = int(currentDetection[2] * width)
			cornerHeight = int(currentDetection[3] * height)
			cornerHorizontal = centerHorizontal - (cornerWidth / 2)
			cornerVertical = centerVertical - (cornerHeight / 2)
			#add to lists 
			classValueList.append(classValue)
			confidences.append(float(confidenceScore))
			boxes.append([cornerHorizontal, cornerVertical, cornerWidth, cornerHeight])

#non-max supression 
indices = cv2.dnn.NMSBoxes(boxes, confidences, CONFIGURE_THRESHOLD, NSMTHRESHOLD)

#drwaing box
for idx in indices:
	idx = idx[0]
	box = boxes[idx]

	cornerHorizontal = box[0]
	cornerVertical = box[1]
	cornerWidth = box[2]
	cornerHeight = box[3]

	#get lable
	label = str(classes[classValue]) 
	print(label)
	#colors for box and text
	randomColor= [[255,0,0],[0,255,0],[0,0,255],[255,100,10]]
	color = randomColor[classValue]
	#get rectangle dimentions. top left coordinates (x,y) and bottom right coordinates (x,y)
	topLeftCornerX = int(cornerHorizontal)
	topLeftCornerY = int(cornerVertical+300)
	bottomRightCornerX = int(cornerHorizontal+cornerWidth)
	bottomRightCornerY = int(cornerVertical+cornerHeight-300)
	#coordinates for text
	coordinatesTextX = int(cornerHorizontal + 150)
	coordinatesTextY = int(cornerVertical + 350 )

	#draw and put text
	cv2.rectangle(image, (topLeftCornerX,topLeftCornerY), (bottomRightCornerX,bottomRightCornerY), color, BORDERTHICKNESS)
	cv2.putText(image, label, (coordinatesTextX,coordinatesTextY), cv2.FONT_HERSHEY_SIMPLEX, TEXTSIZE, color, 2)


#writing the new image to a file
cv2.imwrite("AfterDetection.jpg", image)
cv2.destroyAllWindows()
