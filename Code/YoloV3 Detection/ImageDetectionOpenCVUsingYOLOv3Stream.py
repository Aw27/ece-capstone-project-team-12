# #####################################
# #	Object detection using openCV and pre-trained yolov3 model using IMAGE input 
# # Capstone 2018-2019
# #	Abdullah Barghouti 
# #####################################


#import libraries
import cv2
import argparse
import numpy as np

#constant variables 
#Drawing 
BORDERTHICKNESS = 15
TEXTSIZE = 2
#Detection 
SCALINGFACTOR = 1/255
CONFIGURE_THRESHOLD = 0.5
NSMTHRESHOLD = 0.4
CONFIDENCE = 0.05 

#defintions/ functions 
def getParam():
	global args
	#taking in necessary files from command line as arguments
	ap = argparse.ArgumentParser()
	#ap.add_argument('-i', '--image', required=True, help = 'path to input image')
	ap.add_argument('-c', '--config', required=True, help = 'path to yolo config file')
	ap.add_argument('-w', '--weights', required=True, help = 'path to yolo pre-trained weights')
	ap.add_argument('-cl', '--classes', required=True, help = 'path to text file containing class names')
	args = ap.parse_args()

def loadClasses():
	global classes
#read and load classes from file 
	classes = None 
	with open(args.classes, 'r') as file:
		classes = [line.strip() for line in file.readlines()]
		#number of classes available 
		classesLength = len(classes)
		print(classes)
		print(classesLength)
	return classesLength

#setup and apply neueral Netowrk  
def DeepNeueralNetowrkParameters(arguments, img, scale):
	#global createdNetwork 
	#global prepInput 
	global layers
	
	createdNetwork = cv2.dnn.readNet(args.weights, args.config)
	#this creates the input to the neural network
	prepInput = cv2.dnn.blobFromImage(image, SCALINGFACTOR, (608,608),  crop=False)
	createdNetwork.setInput(prepInput)
	layerName = createdNetwork.getLayerNames()
	#get every unconnected output layer in the list of layers
	outPutLayer = [layerName[idx[0] - 1] for idx in createdNetwork.getUnconnectedOutLayers()]
	#forward pass. inputs passed through the network
	#find name of output layer. In YOLO there are multiple layers giving their predeictions
	layers = createdNetwork.forward(outPutLayer)

def predictAndDraw(imageRecieved):
	for layer in layers:
			#loop for each detection 
			for currentDetection in layer:
                                
				#get confidence and classID. We dont care about the first 5 elements in the list
				#first elements are used for drawing the bounding corners
				#print(currentDetection)
				scores = currentDetection[5:]
				classValue = np.argmax(scores)
				confidenceScore = scores[classValue]
				# print(confidenceScore)
				#look for object
				if confidenceScore > CONFIDENCE:
					#if object was detected ith confidence score hgiher than we specify,
					#find out where it is
					#bounding corners 
					centerHorizontal = int(currentDetection[0] * width)
					centerVertical = int(currentDetection[1] * height)
					cornerWidth = int(currentDetection[2] * width)
					cornerHeight = int(currentDetection[3] * height)
					cornerHorizontal = centerHorizontal - (cornerWidth / 2)
					cornerVertical = centerVertical - (cornerHeight / 2)
					#add to lists 
					classValueList.append(classValue)
					confidenceList.append(float(confidenceScore))
					boxeList.append([cornerHorizontal, cornerVertical, cornerWidth, cornerHeight])

	#non-max supression. 
	#If there are multiple boxes in the same area (or in a small proximity), join them into one 
	indices = cv2.dnn.NMSBoxes(boxeList, confidenceList, CONFIGURE_THRESHOLD, NSMTHRESHOLD)

	#drwaing box
	for idx in indices:
                idx = idx[0]
                box = boxeList[idx]
                #colors for box and text
                randomColor= [[255,0,0],[0,255,0],[0,0,255],[255,100,10]]
                color = randomColor[classValue]
                #draw and put text
                cornerHorizontal = box[0]
                cornerVertical = box[1]
                cornerWidth = box[2]
                cornerHeight = box[3]

                #coordinates for text
                coordinatesTextX = int(cornerHorizontal + 150)
                coordinatesTextY = int(cornerVertical + 350 )
                cv2.rectangle(imageRecieved, (round(cornerHorizontal),round(cornerVertical)), (round(cornerHorizontal+cornerWidth),round(cornerVertical+cornerHeight)), color, BORDERTHICKNESS)
                #cv2.rectangle(imageRecieved, (topLeftCornerX,topLeftCornerY), (bottomRightCornerX,bottomRightCornerY), color, BORDERTHICKNESS)
                #get correct label
                label = str(classes[classValue])
                print ("this is the current label: ", label)
                cv2.putText(imageRecieved, label, (coordinatesTextX,coordinatesTextY), cv2.FONT_HERSHEY_SIMPLEX, TEXTSIZE, color, 2)

#taking in necessary files from command line as arguments
getParam()
#geting image 
loadClasses()
#read image 
cam = cv2.VideoCapture(0)	#CHANGE THIS TO 1 IF USING A COMPUTER WITH A BUILT IN CAMERA 
							#AND YOU WANT TO USE A CAMERA CONNECTED VIA USB
while cv2.waitKey(1) < 0:
	ret, image = cam.read()
	if ret != True:
		print("ERROR! CAMERA COULD NOT OPEN")
		break;
	#get dimentions of image
	height,width = image.shape[:2]

	#list and refresh initialization 
	classValueList = []
	confidenceList = []
	boxeList = []

	#deep neural network 
	DeepNeueralNetowrkParameters(args,image,SCALINGFACTOR)
	#loop for each layer 
	predictAndDraw(image)
	#show image
	cv2.imshow("user input",image)
	
#if user presses q, quit
	if (cv2.waitKey(1) & 0xFF == ord('q')):
		cv2.destroyAllWindows()
		break
