Abdullah Barghouti 
Portland State University 
Capstone 2019

How to run YOLOv3 detection using Python3 and openCV

----Image mode----
1. Make sure all the required files are in the same directory
this means, having the image you want to run the detection on, 
the config file, the weights, and classes file.

2. run the program by typing:
python3 ImageDetectionOpenCVUsingYOLOv3IMAGE.py --image door3.jpg --weights doors-yolov3-tiny_3000.weights --config config.cfg --classes classes.txt

3. An image with called "AfterDetection.jpg" should be created in the same directory

NOTE: all arguments are required to run the program


----Stream mode----
1. Make sure all the required files are in the same directory
this means, having the config file, the weights, and classes file.

2. run the program by typing:
python3 ImageDetectionOpenCVUsingYOLOv3Stream.py --weights doors-yolov3-tiny_3000.weights --config config.cfg --classes classes.txt

3. To quit you can click q or control-c

NOTE: 
- all arguments are required to run the program
- might require you to change video capture argument depending on type of camera (built in vs USB)
