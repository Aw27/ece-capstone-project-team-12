#!/usr/bin/python
# Iain McDonald
# 2/17/2019
# This script was used to copy all labeled images to the master image directory.

import os
import sys
import re
from shutil import copyfile

photofolder = '//home//iain//Downloads/Abdullah'
targetdir = '//home//iain//Documents//001'

for root, subFolder, files in os.walk(photofolder):
    i = 0
    for file in files:
        if('.txt' in file and "classes.txt" not in file):
            photo = re.sub('.txt', '.jpg', file)
            copyfile(os.path.join(root, file), (targetdir + "//" + os.path.split(file)[1]))
            copyfile(os.path.join(root, photo), (targetdir + "//" + os.path.split(photo)[1]))
#print(filelookup)
print('done copying')
