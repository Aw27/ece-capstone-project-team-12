#!/usr/bin/python
# Iain McDonald
# 02/18/2019
# This script was used to sync image label indexes.
# This was needed because labelimg has a default classes file with 14 labels.
# This causes an index offset of 14. 
import os
import sys
import re
from shutil import copyfile

photofolder = '//home//iain//Documents//Austin'
targetdir = '//home//iain//Documents//001'
linearr = []
for root, subFolder, files in os.walk(photofolder):
    for file in files:
        if('.txt' in file and "classes.txt" not in file):
            # Read in file line by line, adding corrected lines to array
            with open(os.path.join(root, file), 'r') as readfile:
                for line in readfile:
                    # Index 15 was open door
                    if line.startswith('15'):
                        linearr.append(re.sub('^15', '1', line))
                    # Index 16 was closed door
                    if line.startswith('16'):
                        linearr.append(re.sub('^16', '0', line))
                    if line.startswith('17'):
                        print(file)
                readfile.close()
            # Dump array to original file name
            if linearr:
                with open(os.path.join(root, file), 'w') as writefile:
                    for line in linearr:
                        writefile.write(line)
                linearr = []
#print(filelookup)
print('done fixing')
