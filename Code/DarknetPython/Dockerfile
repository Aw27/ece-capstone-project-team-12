FROM alpine:3.9.3

RUN apk add --update --no-cache \
    build-base \
    openblas-dev \
    alpine-sdk \
    binutils \
    bzip2 \
    cmake \
    curl \
    gfortran \
    git \
    gcc \
    libavc1394-dev \
    libc6-compat \
    libgphoto2-dev \
    libtheora-dev \
    ltb-project-ssp \
    libjpeg \
    libjpeg-turbo-dev \
    tiff-dev \
    qt-dev \
    ffmpeg \
    ffmpeg-dev \
    clang-dev \
    linux-headers \
    make \
    musl-dev \
    protobuf \
    python2-dev \
    py2-numpy \
    py2-pip \
    py-numpy-dev \
    unzip \
    wget \
    x264 \
    v4l-utils \
    yasm \
    gtk+2.0-dev \
    pkgconfig \
    mesa \
    mesa-gl \
    qt5-qtbase \
    qt5-qtdeclarative \
    glib-dev \
    python2-tkinter \
    tk-dev \
    tk \
    tcl-dev \
    tcl \
    fontconfig \
    fontconfig-dev \
    python3 \
    python3-dev \ 
    python3-tkinter \
    py3-numpy \
    py3-pip \
    xvfb
    
# Change compiler to Clang to avoid compilation errors 
ENV CC /usr/bin/clang
ENV CXX /usr/bin/clang++

# Install opencv
WORKDIR /opencv
RUN wget -O opencv.zip https://github.com/opencv/opencv/archive/3.4.0.zip
RUN wget -O opencv_contrib.zip https://github.com/opencv/opencv_contrib/archive/3.4.0.zip
RUN unzip opencv.zip
RUN unzip opencv_contrib.zip
WORKDIR /opencv/opencv-3.4.0/build
RUN cmake -D CMAKE_BUILD_TYPE=RELEASE \
      -D CMAKE_INSTALL_PREFIX=/usr \
      -D INSTALL_C_EXAMPLES=OFF \
      -D INSTALL_PYTHON_EXAMPLES=OFF \
      -D WITH_TBB=ON \
      -D WITH_V4L=ON \
      -D WITH_QT=OFF \
      -D WITH_OPENGL=ON \
      -D OPENCV_EXTRA_MODULES_PATH=/opencv/opencv_contrib-3.4.0/modules \
      -D BUILD_DOCS=OFF \
      -D BUILD_PERF_TESTS=OFF \
      -D BUILD_TESTS=OFF \
      -D BUILD_opencv_apps=OFF \
      -D BUILD_NEW_PYTHON_SUPPORT=ON \
      -D BUILD_opencv_python3=ON \
      -D HAVE_opencv_python3=ON \
      -D PYTHON_DEFAULT_EXECUTABLE= usr/bin/python3 \
      -D BUILD_EXAMPLES=OFF ..
RUN make
RUN make install
WORKDIR /

# Cleanup
RUN rm -rf /opencv

# Install darknet
RUN git clone https://github.com/pjreddie/darknet.git /darknet
WORKDIR /darknet
RUN git checkout f6d861736038da22c9eb0739dca84003c5a5e275
RUN echo '#include <sys/select.h>' > examples/go.c.updated
RUN sed -i 's/OPENMP=0/OPENMP=1/' Makefile
RUN cat examples/go.c >> examples/go.c.updated
RUN mv examples/go.c.updated examples/go.c
RUN make

# Makes sure opencv is copied to python 3.6
RUN cp -s /usr/lib/python2.7/cv2.so /usr/lib/python3.6/site-packages/cv2.so
# Alexa integration library
RUN python2 -m pip install ask-sdk

# Copy necessary files to docker image
COPY demo.py /darknet/
COPY darknet_libwrapper.py /darknet/
COPY doors.data /darknet/
COPY doors.names /darknet/
COPY doors-calcedanchors-yolov3-tiny.cfg /darknet/
COPY doors-calcedanchors-yolov3-tiny_8000.weights /darknet/
CMD ["python2", "/darknet/demo.py"]
