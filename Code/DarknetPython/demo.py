#Original source: https://github.com/sharowyeh/darknet/blob/master-pjreddie/python/demo.py
"""
Represent demo module from src/demo.c
Functions given camera index or media file for frame detection
"""

import ctypes
from darknet_libwrapper import *
import cv2
import numpy as np
from Tkinter import *
import os
import sys
import stat
import os.path
import datetime
from ask_sdk import *

#Main loop that is called from bottom of script
class Window(Frame):
    def __init__(self, master=None):
        #Init and "global" variables
        Frame.__init__(self, master)
        self.init_window()
        self.alphabet = load_alphabet()
        
    def init_window(self):
        #Window title
        self.master.title("Security System")
        self.pack(fill=BOTH, expand=1)
        #Create a text box to enter the camera path
        campath = Entry(self)
        campath.insert(END, 0)
        campath.place(x = 90, y = 10, width = 150, height = 30)
        #Labels
        label1 = Label(self, text = "Cam Path:").place(x = 20, y = 20, width = 60, height = 30)
        label2 = Label(self, text = "(0 for USB cam or IP address)").place(x = 10, y = 40, width = 200, height = 30)
        #Buttons
        livefeedbutton = Button(self, text="Run Live Feed", command=lambda: self.demo(campath.get())).place(x = 110, y = 70, width = 130, height = 30)
        alexabutton = Button(self, text="Alexa Mode", command=lambda: self.demo(campath.get())).place(x = 0, y = 70, width = 100, height = 30)

    def array_to_image(self, arr):
        arr = arr.transpose(2,0,1)
        c = arr.shape[0]
        h = arr.shape[1]
        w = arr.shape[2]
        arr = (arr/255.0).flatten()
        data = c_array(ctypes.c_float, arr)
        im = IMAGE(w,h,c,data)
        return im

    def ipl_to_image(self, ipl):
        im = self.array_to_image(ipl)
        rgbgr_image(im)
        return im

    def image_to_ipl(self, im):
        rgbgr_image(im)
        buff = np.ctypeslib.as_array(im.data, shape=(im.c, im.h, im.w))
        buff = (buff * 255.0).astype(np.uint8)
        buff = buff.transpose(1,2,0)
        return buff

    def _detector(self, net, meta, image, thresh=.5, hier=.5, nms=.45):
        cuda_set_device(0)
        num = ctypes.c_int(0)
        num_ptr = ctypes.pointer(num)
        network_predict_image(net, image)
        dets = get_network_boxes(net, image.w, image.h, thresh, hier, None, 0, num_ptr)
        num = num_ptr[0]
        if (nms):
             do_nms_sort(dets, num, meta.classes, nms)

        res = []
        for j in range(num):
            for i in range(meta.classes):
                if dets[j].prob[i] > 0:
                    b = dets[j].bbox
                    # Notice: in Python3, mata.names[i] is bytes array from c_char_p instead of string
                    res.append((meta.names[i], dets[j].prob[i], (b.x, b.y, b.w, b.h)))
                    # Begins from yolov3, bbox coords has been changed to % of image's width and height
                    # via network_predict(), remember_network() and avg_predictions()
                    # TODO: solve this workaround for coords via get_network_boxes()
                    dets[j].bbox.x = b.x / image.w
                    dets[j].bbox.w = b.w / image.w
                    dets[j].bbox.y = b.y / image.h
                    dets[j].bbox.h = b.h / image.h
        res = sorted(res, key=lambda x: -x[1])
        draw_detections(image, dets, num, thresh, meta.names, self.alphabet, meta.classes)
        free_detections(dets, num)
        return res

    def demo(self, campath):
        #Load required data for darknet
        meta = get_metadata("doors.data")
        net = load_network("doors-calcedanchors-yolov3-tiny.cfg", "doors-calcedanchors-yolov3-tiny_8000.weights", 0)
        set_batch_network(net, 1)
        
        # If USB cam input, campath needs to be an integer
        if campath == "0":
            cap = cv2.VideoCapture(int(campath))
        else:
            cap = cv2.VideoCapture(campath)
            
        # Get dimensions of input video
        width = cap.get(3)
        height = cap.get(4)
        fps = cap.get(5)

        #Print human readable text to terminal
        print('cap is open?', cap.isOpened(), width, height)
        print('\n\nPress ESC to exit livestream')
        print('\nLogs are saved to /var/log/detectionlogs on the host computer')

        #Create the OpenCV live feed window
        cv2.namedWindow('Video Feed - Press ESC to exit', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('Video Feed - Press ESC to exit', int(width), int(height))
        
        #Get current date and open log file so it can be written to
        curtime = datetime.datetime.now()
        curdate = str(curtime.year) + "_" + str(curtime.month) + "_" + str(curtime.day)
        with open(r"/detectionlogs/" + curdate + ".txt", "a+") as outputfile:
            #Open video stream
            while cap.isOpened():
                #Grab frame from video
                ret, frame = cap.read()
                #If no video, break
                if frame is None:
                    break
                im = self.ipl_to_image(frame)
                result = self._detector(net, meta, im)
                disp = self.image_to_ipl(im)
                #Display video feed + drawn prediction boxes/labels
                cv2.imshow('Video Feed - Press ESC to exit', disp)
                #If there is an object detected, get the current date and time, write to detection log
                if result:
                    curtime = datetime.datetime.now()
                    curtime2 = str(curtime.year) + "-" + str(curtime.month) + "-" + str(curtime.day) + "    " + str(curtime.hour) + ":" + str(curtime.minute) + ":" + str(curtime.second)
                    outputfile.write(curtime2 + "    Result: " +str(result[0][0]) + "\n")
                key = cv2.waitKey(1)
                if key == 27:
                    outputfile.close()
                    break
                
        #Cleanup OpenCV windows
        cv2.destroyAllWindows()
        cap.release()
        
    def alexa(self, campath):
        # Alexa integration code will go here
        # https://alexa-skills-kit-python-sdk.readthedocs.io/en/latest/DEVELOPING_YOUR_FIRST_SKILL.html
        # may help? Mentions opencv: https://devpost.com/software/alexa-home-automation-smart-pi-yw506t
        print("hello")

#Defines the GUI window size, runs main loop of GUI
root = Tk()
root.geometry("250x110") 
app = Window(root)
app.mainloop()
