#!/usr/bin/python
# Iain Mcdonald 2/16/2019
# This program will create test and train files.
# Takes photo folder path as an argument.

import os
import re
from argparse import ArgumentParser


def createfile(current_dir):
    testlist = []
    trainlist = []
    counter = 1
    for root, subFolder, files in os.walk(current_dir):
        for file in files:
            if('.jpg' in file):
                if counter % 10 == 0:
                    testlist.append(os.path.join(root, file) + "\n")
                    counter = counter + 1
                else:
                    trainlist.append(os.path.join(root, file) + "\n")
                    counter = counter + 1
        with open('test.txt', 'w') as writefile:
            for item in testlist:
                writefile.write(item)
            writefile.close()
        with open('train.txt', 'w') as writefile:
            for item in trainlist:
                writefile.write(item)
            writefile.close()
            
parser = ArgumentParser()
parser.add_argument("-d", "--dir", type=str,
                    help="path to photo dir", metavar="DIR")
args = vars(parser.parse_args())
current_dir = args['dir']
if current_dir:
    createfile(current_dir)
else:
    parser.print_help()
