MUST:
	Use a camera to detect if window/door is closed/open and locked/unlocked
	Recognize if open/closed or locked/unlocked status has changed
	Use commercial off-the-shelf (COTS) components
SHOULD:
	Intercept IP camera instead of USB webcam
	Utilize existing SW/cloud frameworks
MAY:
	Integrate voice recognition system to query door/window status
	